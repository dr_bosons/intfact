#!/usr/bin/python3

import sys
from mpmath import *
from pi import pi
from e import e
from zeros import zeros

def characterize(num):
    l = len(num)
    ctr = 0
    pcnt = 0
    zsum = 0.0
    pzsum = 0.0
    pivots = []
    pnk = -1
    while ctr < l:
        nk = num[ctr]
        pk = pi[pcnt]
        ek = e[pcnt]
        if int(pk+ek) == 0:
            zz = 100
        else:
            zz = int(pk+ek)
        zero  = str(zetazero(zz).imag)
        idx = zero.index(".")
        zero = zero[idx-1:idx+4]
        zsum = zsum + float(zero)
        if pzsum > 0:
            pzsum = pzsum + float(zero)
        if pk == pnk and pnk != nk:
            if int(pzsum) in zeros:
                pivots.append([pnk,pcnt])
            pcnt = pcnt + 1
            continue
        if pk == nk:
            if int(zsum) in zeros:
                pivots.append([nk,pcnt])
                pzsum = zsum
                zsum = 0
                pnk = nk
                ctr = ctr + 1
                pcnt = pcnt + 1
                continue
        pcnt = pcnt + 1
    return pivots,pcnt

if __name__ == "__main__":
    num = str(sys.argv[1])
    print("Number to be factored : " + str(num))
    pivots,pcnt = characterize(num)
    print(pi[:pcnt])
    print(e[:pcnt][::-1])
    print(pivots)
