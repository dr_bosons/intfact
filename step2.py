#!/usr/bin/python3

import sys
from mpmath import *
from pi import pi
from e import e

if __name__ == "__main__":
    mp.prec = 200
    mp.dps = 200
    for x in range(6001, 20001):
        zero = str(zetazero(x).imag)
        idx = zero.index(".")
        zero = zero[idx-1:idx+10]
        pk = pi[x-1]
        print(str(int(pk)), '\t', str(zero))
