#!/usr/bin/python3

import sys
from mpmath import *
from pi import pi
from e import e
LIMIT = 200

def partition(num):
    tuples = []
    ctr = 0
    l = len(num)
    while True:
       ak = ""
       for x in range(0, 2):
         ak = ak + num[(ctr + x) % l]
       ctr = ctr + 2
       bk = ""
       for x in range(0, 2):
         bk = bk + num[(ctr + x) % l]
       tuples.append([ak, bk])
       if ctr >= l:
           break
       ctr = ctr - 1
    return tuples

def find_matching_pairs(rnum, tuples, pp):
    global LIMIT
    mp.prec = LIMIT
    mp.dps = LIMIT
    ctr = 1
    match_count = 0
    snippets = []
    while True:
        """
        if ctr % 100 == 0:
            print(str(ctr)+ "...")
        """
        pk = pp[ctr-1]
        rk = rnum[match_count]
        if pk == rk:
            tk1 = tuples[match_count][0]
            tk2 = tuples[match_count][1]
            zero = str(zetazero(ctr).imag)
            idx = zero.index(".")
            zero = zero[idx-1:idx+10]
            zero = zero.replace(".","")
            if tk1 in zero and tk2 in zero:
                index1 = zero.index(tk1)
                index2 = zero.index(tk2)
                while tk2 in zero[index2+1:]:
                    index2 = zero.index(tk2, index2+1)
                if index2 <= index1+2:
                    ctr = ctr + 1
                    continue
                match_count = match_count + 1
                zz = zero[index1+2:index2]
                print([ctr, zero, tk1, tk2, zz])
                snippets.append(zz)
                ctr = 1
                if match_count >= len(tuples):
                    break
                continue
            else:
                ctr = ctr + 1
                continue
        ctr = ctr + 1
    return snippets

def print_factors(pp_matching_pairs, tuples, pp):
    pass

if __name__ == "__main__":
    num = str(sys.argv[1])
    print("Number to be factored: " + str(num))
    rnum = num[::-1]
    tuples = partition(num)
    pi_matching_pairs = find_matching_pairs(rnum, tuples, pi)
    print(pi_matching_pairs)
#    e_matching_pairs = find_matching_pairs(rnum, tuples, e)
    print_factors(pi_matching_pairs, tuples, pi)
#    print_factors(e_matching_pairs, e)
