#!/usr/bin/python3
import mpmath

def gen_zeros(index):
    mpmath.mp.prec = 8192
    mpmath.mp.dps = 8192
    zz =  str(mpmath.zetazero(index).imag)
    index = zz.index(".")
    zz = zz[index+1:]
    return zz
