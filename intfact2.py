#!/usr/bin/python3

import sys
import math
from mpmath import *
from pi import pi
from e import e
LIMIT = 4096
def partition(num, sz):
    triplets = []
    i = 0
    l = len(num)
    ctr = 0
    while ctr < sz:
        ss = ""
        for j in range(0,3):
            ss = ss + num[(i+j) % l]
        triplets.append(ss)
        i = i + 3
        ctr = ctr + 1
    return triplets

def solve(triplets, r_triplets):
    global LIMIT
    mp.prec = LIMIT
    mp.dps = LIMIT
    pivots = []
    sz = len(triplets)
    pcnt = 0
    for j in range(0, sz):
        pivot_triplet = []
        triplet = triplets[j]
        ctr = 0
        while ctr < 3:
            pp = pi[pcnt]
            ee = e[pcnt]
            if pp == triplet[ctr]:
                val = int(pp)*10 + int(ee)
                if val == 0:
                    val = 100
                while (len(pivot_triplet) == 2 and val == pivot_triplet[1]) or (len(pivot_triplet) >= 1 and val == pivot_triplet[0]):
                    pcnt = pcnt + 1
                    pp = pi[pcnt]
                    ee = e[pcnt]
                    val = int(pp)*10 + int(ee)
                    if val == 0:
                        val = 100
                pivot_triplet.append(val)
                ctr = ctr + 1
            pcnt = pcnt + 1
        pivot_triplet_string = []
        len_pivot = 0
        print(pivot_triplet)
        for triplet in pivot_triplet:
            zero = str(zetazero(triplet).imag)
            idx = zero.index(".")
            zero = zero[idx+1:]
            pivot_triplet_string.append(zero)
            len_pivot = len(zero)
        ctr = 0
        succ = False
        while ctr < len_pivot:
            t1 = int(pivot_triplet_string[0][ctr])
            t2 = int(pivot_triplet_string[1][ctr])
            t3 = int(pivot_triplet_string[2][ctr])
            if (t1 == int(r_triplets[j][2])) and (t2 == int(r_triplets[j][1])) and (t3 == int(r_triplets[j][0])):
                succ = True
                _pp = int(pi[j])
                _ee = int(e[ctr-j])
                val = _pp*10+_ee
                print(val)
                pivots.append(val)
                break
            ctr = ctr + 1
        if succ == False:
            print("More precision is needed")
            sys.exit(0)
    return pivots

if __name__ == "__main__":
    num = str(sys.argv[1])
    print("Number to be factored: "+ str(num))
    l = len(num)
    sz = int(math.ceil(l / 3.0)+1)
    triplets = partition(num, sz)
    rnum  = num[::-1]
    r_triplets = partition(rnum, sz)
    print(triplets)
    print(r_triplets)
    pivots = solve(triplets, r_triplets)
    print(pivots)
   # factor1, factor2 = synthesize_factors(pivots)
    
