#!/usr/bin/python

import sys

if __name__ == "__main__":
    num = str(sys.argv[1])
    for x in num[::-1]:
        print(int(x))
