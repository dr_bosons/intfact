#!/usr/bin/python3

import sys

if __name__ == "__main__":
    num = str(sys.argv[1])
    ctr = 0
    l = len(num)
    pairs_of_tuples = []
    while True:
       ak = ""
       for x in range(0, 2):
         ak = ak + num[(ctr + x) % l]
       ctr = ctr + 1
       bk = ""
       for x in range(0, 2):
         bk = bk + num[(ctr + x) % l]
       pairs_of_tuples.append([ak, bk])
       if ctr >= l:
           break
    print(pairs_of_tuples)
