#!/usr/bin/python3

import sys
from mpmath import *

def genZetaZeros(x, d):
    mp.prec = 2*(d+1)+100
    mp.dec = 2*(d+1)+100
    zero = zetazero(x).imag
    #print([x, zero])
    idx = str(zero).index(".")
    zero = str(zero)[idx-1:]
    #print(zero)
    zero = zero.replace(".","")
    zz = zero[d:d+2]
    zz = zz[0] + "." + zz[1]
    #print(zz)
    return float(zz)
