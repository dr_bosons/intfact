#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <gmp.h>
#include "pi.h"
#include "e.h"
#include "zetazeros.h"
#define PY_SSIZE_T_CLEAN
#include <Python.h>
#define EXPANSION_FACTOR 3//10
#define FLOAT_LENGTH 7

char* strrev(char* str) {
	int l = strlen(str);
	int i = 0;
	char* _str = strdup(str);
	while (i < (l+1)/2) {
		char t = _str[i];
		_str[i] = _str[l - 1 - i];
		_str[l - 1 - i] = t;
		++i;
	}
	return _str;
}

short int match(char* sum,  char** pp, int* match_cnt, int* indices, int* index, int t) {
	char* ptr1 = strchr(sum, '.');
	if (ptr1) {
		*(ptr1 + 3) = '\0';
		if (*(ptr1 + 2) == '0') {
			*(ptr1 + 2) = '\0';
		}
		if (*(ptr1 + 1) == '0') {
			*(ptr1 + 1) = '\0';
		}
	}
	int l_sum_str = strlen(sum);
	char* copy_str = (char*) calloc(l_sum_str+1, sizeof(char));
	int j = 0;
	for (int i = 0; i < l_sum_str; ++i ) {
		if(sum[i] == '.') continue;
		copy_str[j++] = sum[i];
	}	       
	copy_str[j] = '\0';
	char* ptr = strstr(*pp, copy_str);
	char* _copy_str = strrev(copy_str);
	char* ptr2 = strstr(*pp, _copy_str);
	//	printf("\nt: %d\tcopy_str: %s\t_copy_str: %s\n",t, copy_str, _copy_str);
	if (t == 0 && ptr2 && (ptr2-(*pp)) == 0) {
	//	printf("\n_copy_str : %s\n",_copy_str);
		int new_match_cnt = *match_cnt + strlen(_copy_str);
		indices[*index] = new_match_cnt;
		*match_cnt = new_match_cnt;
		*pp = *pp + strlen(_copy_str);
		*index = *index + 1;
		//system("a=1;read a");
		return 1;
	} else if (t == 1 && ptr && (ptr-(*pp)) == 0) {
	//	printf("\ncopy_str : %s\n",copy_str);
		int new_match_cnt = *match_cnt + strlen(copy_str);
		indices[*index] = new_match_cnt;
		*match_cnt = new_match_cnt;
		*pp = *pp + strlen(copy_str);
		*index = *index + 1;
		//system("a=1;read a");
		return 1;
	}
	return 0;
}

void characterize(char* num, FILE* f_pi, FILE* f_e, FILE* pi_out, FILE* e_out, FILE* pivots, FILE* match_sums, FILE* digits) {
	int ctr = 0;
	int l = strlen(num);
	const char* const_pi = pi;
	const char* const_e = e;
	int match_cnt = 0;
	int first = 1;
	int pnk = -1;
	char* pp = 0;
	int t = 0;
	int index = 0;
	int* indices = (int*) calloc(EXPANSION_FACTOR*l+1,sizeof(int));
	char nk = 0;
	while (ctr < l) {
		int nread = 0;
		char pk = 0;
		char ek = 0;
		char* p_try = (char*) calloc(EXPANSION_FACTOR*l+1, sizeof(char));
		char* e_try = (char*) calloc(EXPANSION_FACTOR*l+1, sizeof(char));
		int* idx_try = (int*) calloc(EXPANSION_FACTOR*l+1, sizeof(int));
		int* file_indices = (int*) calloc(EXPANSION_FACTOR*l+1, sizeof(int));
		int trial_count = ctr;
		int trial_pnk = pnk;
		int succ = 0;
		while ((fscanf(f_pi, "%c", &pk) != EOF) && (fscanf(f_e, "%c", &ek)!= EOF)) {
			nk = num[trial_count];
			if (pk == '.' && ek == '.') {
				fscanf(f_pi, "%c", &pk);
				fscanf(f_e, "%c", &ek);
			}
			//			printf("\n%ld\tctr: %d\ttrial_cnt: %d\tnk=%c\tpnk=%c\tpk=%c\tek=%c\n",ftell(f_pi),ctr, trial_count,nk,pnk, pk, ek);
			if (pnk > -1  && pnk != nk && pnk == pk)  {
				succ = 1;
				p_try[nread]= pk;
				idx_try[nread] = trial_count-1;
				file_indices[nread]=ftell(f_pi)-1;
				e_try[nread++] = ek;
			} else if (nk == pk) {
				succ = 1;
				pnk = nk;
				idx_try[nread] = trial_count;
				p_try[nread]= pk;
				file_indices[nread]=ftell(f_pi)-1;
				e_try[nread++] = ek;
				++trial_count;
			} else {
				break;
			}
		}
		if (succ == 0) {
			pnk = trial_pnk;
			if (nread > 0) {
				fseek(f_pi, -nread, SEEK_CUR);
				fseek(f_e, -nread, SEEK_CUR);
			}
			free(p_try);
			free(file_indices);
			free(e_try);
			free(idx_try);
			continue;
		}
		p_try[nread] = e_try[nread] = '\0';
		int read_l = strlen(p_try);
		char* sum = (char*) calloc(FLOAT_LENGTH, sizeof(char));
		sum[0] = '0';
		sum[1] = '.';
		sum[2] = '0';
		sum[3] = '\0';
		/*matching process*/
		PyObject *pName, *pModule, *pFunc;
		PyObject *pArgs, *pValue;
		int cnt = 0;
		while (cnt < read_l) {
			char ck[3];
			ck[0] = p_try[cnt];
			ck[1] = e_try[cnt];
			ck[2] = '\0';
			int cn = atoi(ck);
			if (cn == 0) cn = 100;
			char* zetazero_str = (char*) calloc(FLOAT_LENGTH, sizeof(char));//zetazeros[cn-1];
			double pVal = 0.0;
			Py_Initialize();
			PyRun_SimpleString("import sys");
			PyRun_SimpleString("sys.path.append(\".\")");
			PyRun_SimpleString("sys.path.append(\"/usr/local/lib/python3.8/dist-packages\")");
			pName = PyUnicode_DecodeFSDefault("genZeros");
			pModule = PyImport_Import(pName);
			Py_DECREF(pName);
			if (pModule != NULL) {
				pFunc = PyObject_GetAttrString(pModule, "genZetaZeros");
				pArgs = PyTuple_New(2);
				pValue = PyLong_FromLong(cn);
				PyTuple_SetItem(pArgs, 0, pValue);
				pValue = PyLong_FromLong(idx_try[cnt]);
				PyTuple_SetItem(pArgs, 1, pValue);
				pValue = PyObject_CallObject(pFunc, pArgs);
				Py_DECREF(pArgs);
				pVal = PyFloat_AS_DOUBLE(pValue);
				//printf("Result of call: %f\n", pVal);
				Py_DECREF(pValue);
				Py_XDECREF(pFunc);
				Py_DECREF(pModule);
			}
			Py_FinalizeEx();
			sprintf(zetazero_str, "%.2f", pVal);
			char* chr = strchr(zetazero_str, '.');
			const char* ss = zetazero_str;
			int int_part = 0;
			int frac_part = 0;
			while (ss != chr) {
				int_part = int_part*10 + (*ss - '0');
				++ss;
			}
			++ss;
			while (*ss != '\0') {
				frac_part = frac_part*10 + (*ss - '0');
				++ss;
			} 
			chr = strchr(sum, '.');
			ss = sum;
			int int_sum_part = 0;
			int frac_sum_part = 0;
			while ((!chr  && *ss != '\0') || (chr && ss != chr)) {
				int_sum_part = int_sum_part*10 + (*ss - '0');
				++ss;
			}
			++ss;
			while (*ss != '\0') {
				frac_sum_part = frac_sum_part*10 + (*ss - '0');
				++ss;
			}
			int_sum_part += int_part;
			frac_sum_part += frac_part;
			if (frac_sum_part > 99) {
				int_sum_part++;
				frac_sum_part %= 100;
			}
			if (int_sum_part > 10)  {
				sum[0] = (int_sum_part / 10) + '0';
				sum[1] = (int_sum_part % 10) + '0';
				sum[2] = '.';
				if (frac_sum_part > 10) {
					sum[3] = (frac_sum_part / 10) + '0';
					sum[4] = (frac_sum_part % 10) + '0';
					sum[5] = '\0';
				} else {
					sum[3] = (frac_sum_part % 10) + '0';
					sum[4] = '\0';
				}
			} else {
				sum[0] = (int_sum_part % 10) + '0';
				sum[1] = '.';
				if (frac_sum_part > 10) {
					sum[2] = (frac_sum_part / 10) + '0';
					sum[3] = (frac_sum_part % 10) + '0';
					sum[4] = '\0';
				} else {
					sum[2] = (frac_sum_part % 10) + '0';
					sum[3] = '\0';
				}

			}
			if (first == 1) {
				//				printf("\n Sum 1: %s\n" , sum);
				int mat_bool1 = match(sum, (char**)&pi, &match_cnt, indices, &index, t);
				int mat_bool2 = match(sum, (char**)&e, &match_cnt, indices, &index, t);
				if (mat_bool1 == 1) {
					fprintf(match_sums, "%s\n", sum);
					for (int j = 0; j < nread; ++j) {
						fprintf(pivots, "%d\n", file_indices[j]);
						fprintf(digits, "%c\n", p_try[j]);
					}
					pp = (char*) pi;
					first = 0;
					t = 1 - t;
					succ = 1;
					break;
				} else if (mat_bool2 == 1) {
					fprintf(match_sums, "%s\n", sum);
					for (int j = 0; j < nread; ++j) {
						fprintf(pivots, "%d\n", file_indices[j]);
						fprintf(digits, "%c\n", p_try[j]);
					}
					pp = (char*) e;
					first = 0;
					t = 1 - t;
					succ = 1;
					break;
				} else {
					succ = 0;
				}
			} else {
				//				printf("\n Sum 2: %s\n" , sum);
				int bool1 = match(sum, &pp, &match_cnt, indices, &index, t);
				if (bool1 == 1) {
					fprintf(match_sums, "%s\n", sum);
					for (int j = 0; j < nread; ++j) {
						fprintf(pivots, "%d\n", file_indices[j]);
						fprintf(digits, "%c\n", p_try[j]);
					}
					t = 1 - t;
					succ = 1;
					break;
				} else {
					succ = 0;
				}
			}
			++cnt;
		}
		if (succ == 0) {
			pnk = trial_pnk;
			if (nread > 0) {
				fseek(f_pi, -nread, SEEK_CUR);
				fseek(f_e, -nread, SEEK_CUR);
			}
		} else {
			ctr = idx_try[--nread]+1;
		}
		free(p_try);
		free(e_try);
		free(file_indices);
	}
	free(indices);
	for (int k = 0; k < ftell(f_pi)-2; ++k) {
		fprintf(pi_out, "%c", const_pi[k]);
	}
	for (int k = 0; k < ftell(f_e)-2; ++k) {
		fprintf(e_out, "%c", const_e[k]);
	}
	return;
}

void factorize(FILE* pi_out, FILE* e_out, FILE* pivots, FILE* factor1) {
	int pivot = 0;
	PyObject *pName, *pModule, *pFunc;
	PyObject *pArgs, *pValue;
	while ((fscanf(pivots, "%d", &pivot) != EOF)) {
		char pk = 0, ek = 0;
		fseek(pi_out, pivot-1, SEEK_SET);
		fseek(e_out, -(pivot+1), SEEK_END);
		fscanf(pi_out, "%c", &pk);
		fscanf(e_out, "%c", &ek);
		int cn = (pk-'0')*10 + (ek - '0');
		if (cn == 0) cn = 100;
		char* zetazero_str = (char*) calloc(2048, sizeof(char));//zetazeros[cn-1];
		double pVal = 0.0;
		Py_Initialize();
		PyRun_SimpleString("import sys");
		PyRun_SimpleString("sys.path.append(\".\")");
		PyRun_SimpleString("sys.path.append(\"/usr/local/lib/python3.8/dist-packages\")");
		pName = PyUnicode_DecodeFSDefault("genZeros2");
		pModule = PyImport_Import(pName);
		Py_DECREF(pName);
		if (pModule != NULL) {
			pFunc = PyObject_GetAttrString(pModule, "genZetaZeros");
			pArgs = PyTuple_New(1);
			pValue = PyLong_FromLong(cn);
			PyTuple_SetItem(pArgs, 0, pValue);
			pValue = PyObject_CallObject(pFunc, pArgs);
			Py_DECREF(pArgs);
			pVal = PyFloat_AS_DOUBLE(pValue);
			//printf("Result of call: %f\n", pVal);
			Py_DECREF(pValue);
			Py_XDECREF(pFunc);
			Py_DECREF(pModule);
		}
		Py_FinalizeEx();
		sprintf(zetazero_str, "%f", pVal);
		fprintf(factor1,"%s\n", zetazero_str);
	}
	return;
}

int main(int argc, char* argv[]) {
	wchar_t *program = Py_DecodeLocale(argv[0], NULL);
	if (program == NULL) {
		fprintf(stderr, "Fatal error: cannot decode argv[0]\n");
		exit(1);
	}
	Py_SetProgramName(program);  /* optional but recommended */
	char* num = strdup(argv[1]);
	printf("\nNumber to be factored : %s\n", num);
	FILE* pi_out = fopen("/media/bosons/bigbasket/Projects/intfact/pi_out.txt","w");
	FILE* e_out = fopen("/media/bosons/bigbasket/Projects/intfact/e_out.txt","w");
	FILE* pivots = fopen("/media/bosons/bigbasket/Projects/intfact/pivots.txt","w");
	FILE* match_sums = fopen("/media/bosons/bigbasket/Projects/intfact/match_sums.txt","w");
	FILE* digits = fopen("/media/bosons/bigbasket/Projects/intfact/digits.txt","w");
	FILE* f_pi = fopen("/media/bosons/bigbasket/Projects/intfact/pi.txt","r");
	FILE* f_e = fopen("/media/bosons/bigbasket/Projects/intfact/e.txt","r");
	characterize(num, f_pi, f_e, pi_out, e_out, pivots, match_sums, digits);
	fprintf(pi_out,"\n");
	fprintf(e_out,"\n");
	fclose(f_e);
	fclose(f_pi);
	fclose(pivots);
	fclose(e_out);
	fclose(pi_out);
	fclose(digits);
	fclose(match_sums);
	pi_out = fopen("/media/bosons/bigbasket/Projects/intfact/pi_out.txt","r");
	e_out = fopen("/media/bosons/bigbasket/Projects/intfact/e_out.txt","r");
	pivots = fopen("/media/bosons/bigbasket/Projects/intfact/pivots.txt","r");
	FILE* factor1 = fopen("/media/bosons/bigbasket/Projects/intfact/factor1.txt","w");
	factorize(pi_out, e_out, pivots, factor1);
	fclose(factor1);
	fclose(pivots);
	fclose(e_out);
	fclose(pi_out);
	PyMem_RawFree(program);
	return 0;
}
